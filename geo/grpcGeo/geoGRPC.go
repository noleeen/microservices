package grpcGeo

import (
	"context"
	"geo/internal/entities/geoEntity"
	"geo/internal/modules/geo/service"
	pb "gitlab.com/noleeen/protogeo/gen/geo"
)

type GeoServicerGRPC interface {
	PrepareGeocodeRequest(c context.Context, request *pb.GeocodeRequest) (*pb.Suggestions, error)
	PrepareSearchRequest(c context.Context, request *pb.SearchRequest) (*pb.SearchResponse, error)
}

type GeoServiceGRPC struct {
	GeoService service.GeoServicer
	pb.UnimplementedGeorerServer
}

func NewGeoServiceGRPC(g service.GeoServicer) *GeoServiceGRPC {
	return &GeoServiceGRPC{GeoService: g}
}

func (g *GeoServiceGRPC) PrepareGeocodeRequest(c context.Context, request *pb.GeocodeRequest) (*pb.Suggestions, error) {
	req := geoEntity.GeocodeRequest{
		Lat: request.Lat,
		Lng: request.Lng,
	}

	geocodeResponse, err := g.GeoService.PrepareGeocodeRequest(req)
	if err != nil {
		return nil, err
	}

	var s []*pb.Suggestion
	for _, val := range geocodeResponse.Suggestions {
		s = append(s, &pb.Suggestion{
			Value: val.Value,
			Data:  val.Data,
		})
	}

	return &pb.Suggestions{Suggestions: s}, nil

}

func (g *GeoServiceGRPC) PrepareSearchRequest(c context.Context, request *pb.SearchRequest) (*pb.SearchResponse, error) {
	req := geoEntity.SearchRequest{Query: request.Query}

	searchResponse, err := g.GeoService.PrepareSearchRequest(req)
	if err != nil {
		return nil, err
	}

	var a []*pb.Address
	for _, val := range searchResponse.Addresses {
		a = append(a, &pb.Address{
			Lat:    val.Lat,
			Lon:    val.Lon,
			Result: val.Result,
		})
	}

	return &pb.SearchResponse{Addresses: a}, nil
}
